var  PI=Math.PI,
     pi=Math.PI,
    abs=Math.abs,
    sin=Math.sin,
   asin=Math.asin,
    cos=Math.cos,
    tan=Math.tan,
   atan=Math.atan,
  atan2=Math.atan2,
  floor=Math.floor,
   ceil=Math.ceil,
    max=Math.max,
    min=Math.min,
 random=Math.random,
  round=Math.round,
   sqrt=Math.sqrt,
    exp=Math.exp,
    log=Math.log,
    pow=Math.pow;

	
current_note = C4;
magic_number = pow(2,(1/12));
magic_number_down = pow(2,(-1/12));
//play_notes(scale, 1000);


function up_the_note() {
	current_note *= magic_number;
}

function down_the_note() {
	current_note *= magic_number_down;
}

function play_the_note() {
	play_note([current_note]);
}


function play_notes(list_of_freqs,delay) {
	list_of_freqs.forEach(function(item) {	
		console.log(item);
		play_note(item);
		sleep(1000);
	});

}

function play_note(frequencies) {

	sampleRate = 44100;
	var samples = [];
	
	var samples_length = 44100;               // Plays for 1 second (44.1 KHz)
	for (var i=0; i < samples_length ; i++) { // fills array with samples
	  var t = i/samples_length; 			  // time from 0 to 1
	  frequencies.forEach(function(freq, index) {
		if(index == 0){
			samples[i] = sin(freq * 2*PI*t);
		}
		else {
			samples[i] += sin(freq * 2*pi*t);
		}
		samples[i] /= frequencies.length;
	  });
	  
	  samples[i] *= .5; //Amplify 
	  samples[i] *= (1-t);                    // "fade" effect (from 1 to 0)
	}


	//normalize_invalid_values(samples); // keep samples between [-1, +1]


	var wave = new RIFFWAVE();
	wave.header.sampleRate = sampleRate;
	wave.header.numChannels = 1;
	var audio = new Audio();
	var samples2=convert255(samples);
	wave.Make(samples2);
	audio.src=wave.dataURI;
	audio.play();
	//setTimeout(function() { audio.play(); }, 1000); // page needs time to load?

} 

function normalize_invalid_values(samples) {
  for (var i=0, len=samples.length; i<len; i++) {
    if (samples[i]>1) {
      samples[i] = 1;
    } else if (samples[i]<-1) {
      samples[i] = -1;
    }
  }
}

function convert255(data) {
  var data_0_255=[];
  for (var i=0;i<data.length;i++) {
    data_0_255[i]=128+Math.round(127*data[i]);
  }
  return data_0_255;
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}