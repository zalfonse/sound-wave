Mousetrap.bind('q', function() { 
	play_note([C4]);
	dropAt((1/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('w', function() { 
	play_note([D4]);
	dropAt((2/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('e', function() { 
	play_note([E4]);
	dropAt((3/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('r', function() { 
	play_note([F4]);
	dropAt((4/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('t', function() { 
	play_note([G4]);
	dropAt((5/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('y', function() { 
	play_note([A4]);
	dropAt((6/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('u', function() { 
	play_note([B4]);
	dropAt((7/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('i', function() { 
	play_note([C5]);
	dropAt((8/8) * width - (1/16) * width, .5 * height);
});

Mousetrap.bind('s', function() { 
	play_the_note();
});

Mousetrap.bind('a', function() { 
	down_the_note();
	play_the_note();
});

Mousetrap.bind('d', function() { 
	up_the_note();
	play_the_note();
});