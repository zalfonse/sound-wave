C4 = 261.63;
C5 = 523.26;
Csharp4 = 277.18;
Csharp5 = 554.37;
D4 = 293.66;
D5 = 587.33;
Dsharp4 = 311.13;
Dsharp5 = 622.25;
E4 = 329.63;
E5 = 659.26;
F4 = 349.23;
F5 = 698.46;
Fsharp4 = 369.99;
Fsharp5 = 739.99;
G4 = 392;
G5 = 783.99;
Gsharp4 = 415.3;
Gsharp5 = 830.61;
A4 = 440;
A5 = 880;
Asharp4 = 466.16;
Asharp5 = 932.33;
B4 = 493.88;
B5 = 987.77;


scale = [[C4], [D4], [E4], [F4], [G4], [A5], [B5], [C5]];